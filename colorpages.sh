#!/bin/sh
# Based on Ulriks answer for https://superuser.com/questions/234408/count-bw-color-pages-in-pdf

# Get input file
file=$1
if [ -z $file ]
then
	echo "No input file"
	return
fi

# Count pages
colorpages=$(gs -o - -sDEVICE=inkcov $file | grep -v "^ 0.00000  0.00000  0.00000" | grep "^ " | wc -l)
totalpages=$(pdfinfo $file | grep "Pages:" | grep -E -o "[[:digit:]]+$")
echo "$colorpages pages colored ($totalpages total)"
